# Hardware.EC-01.02

## Entrada de Comandos

Autor: 

- Germán Fernández

Revisión: 

- Agustín González

Hardware:

- 5 entradas optoaisladas.
- 5 pulsadores para forzar entradas.
- 5 indicadores luminosos de entrada activada.
- Aislación de entradas del PIC.
- Conector para segunda placa IComando o EComandos.

Software:

- Eagle 6.5.0 Light

Usos:

- Semáforo
- Tablero de Comandos